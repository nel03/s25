/*
	JSON object
		> Stand for Javascript Object Notation
		> is also used for other programming languages hence the name Javascrip Object Notation

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

// JSON Object
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// JSON Array

// "cities" = [
// 	{"city": "Quezon City", "province": "Metro manila", "country": "Philippines"},
// 	{"city": "Manila", "province": "Metro manila", "country": "Philippines"},
// 	{"city": "Makaty City", "province": "Metro manila", "country": "Philippines"},
// ]

//  JSON methods

/*
	JSON object contains methods for parsing and converting data into stringified JSON
*/

// converting Data into stringified JSON

let batchArr = [
	{batchName: "Batch 197"},
	{batchName: "Batch 198"}
]

console.log(batchArr)

// The Stringify method is used to convert JS Objects into string. we are doing this berofre sending the data to convert an array or an object to its string equivalent.

console.log('Result from stringify method:')
console.log(JSON.stringify(batchArr)) 

// convert JavaScript to JSON 
let userProfile = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		region: "Metro Manila",
		country: "Philippines"
	}
})

console.log('Result from stringify method (object)')
console.log(userProfile)

// User Details
// let firstName = prompt('What is your first name?')
// let lastName = prompt('What is your last name?')
// let age = prompt('what is your age?')
// let address = {
// 	city: prompt('which city do you live in?'),
// 	country: prompt('which country does your city address belong to?')
// }

// let userData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(userData)


console.log(" ")
/*
	Convert stringified JSON into JS Objects
	JSON.parse()
*/
//JSON format to JavaScript
let batchesJSON = `[
	{
		"batchName": "Batch 197"
	},
	{
		"batchName": "Batch198"
	}

]`

console.log(batchesJSON)

/*
	upon receiving data, the JSON text can be converted into JS objects so that we can use it in our program
*/

console.log('result from parse method:')
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `{
		"name": "Ivy",
		"age": "18",
		"address": {
			"city": "Caloocan City",
			"country": "Philippines"
		}
	}`

console.log(stringifiedObject)
console.log('result from parse method (object):')
console.log(JSON.parse(stringifiedObject))